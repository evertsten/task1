/*
    Here I can see an program for e-store checkout system.
    First the program checks if all requirements are met.
    If everything is OK then it declares variables and submits the payment.
    If payment transaction is successful, purchase order gets confirmed and customer details are sent to the server.
    After that, the program clears the form and dispatches a message to the user.
*/

export const submitOrder = () => // We are creating an export function, so it can be used by other programs.
async function thunk(dispatch, getState) { // Asynchronous Redux function with 2 mandatory arguments.
    dispatch(actions.order.submit()); // Dispatches an action to submit the order.
    const { cart, user } = getState(); // Returns the state(value) of cart & user. 
    const { cartId } = cart; // Sets the cart variable equal to cartId.
        if (!cartId) { // If cartId value is missing.
            throw new Error('Missing required information: cartId'); // Throws error if the condition is true.
        }
    const shipping_address = await retrieveShippingAddress(); /* Declaring new variable using await kayword.
    It makes JS wait until we've retrieveShippingAddress(); */
    const comment = await retrieveComment(); // Makes JS wait until we've retrieveComment();
    let billing_address, paymentMethod; // Declares two new variables.
    cart.paymentMethods.map(payment_method => { // Creates an paymentMethods array, using map() method.
        if (payment_method.code === 'free') { // Checks if payment_method.code is free.
            billing_address = shipping_address; // If payment_method.code is free, then sets billing_address equal to shipping_address.
            paymentMethod = payment_method; // And sets paymentMethod equal to payment_method.
        }
    });
    if (!billing_address) billing_address = await retrieveBillingAddress(); // Makes JS wait until we've retrieveBillingAddress();
    if (!paymentMethod) paymentMethod = await retrievePaymentMethod(); // Makes JS wait until we've retrievePaymentMethod();
    if (billing_address.sameAsShippingAddress) { // If billing_address is same as ShippingAddress
        billing_address = shipping_address; // It Sets the shipping_address same as billing_address.
    }
    try {
        // POST to payment-information to submit the payment
        details and billing address, // tries to submit the payment.
        // Note: this endpoint also actually submits the order.

    const guestPaymentEndpoint = `/rest/V1/guest-
    carts/${cartId}/payment-information`; // Sets the guestPaymentEndpoint variable url based on the cartId value.

    const authedPaymentEndpoint =
    '/rest/V1/carts/mine/payment-information'; // Sets the authedPaymentEndpoint variable.

    // Sets the paymentEndpoint using unique user values.
    const paymentEndpoint = user.isSignedIn
    ? authedPaymentEndpoint
    : guestPaymentEndpoint;

    // Line 40-64 | Sends user data to server using JSON POST request.
    const response = await request(paymentEndpoint, {
        method: 'POST',
        body: JSON.stringify({
            billingAddress: billing_address,
            cartId: cartId,
            email: shipping_address.email,
            paymentMethod: {
            additional_data: {
            payment_method_nonce:
            paymentMethod.data &&
            paymentMethod.data.nonce,
            payment_comment: comment
            },
            method: paymentMethod.code
            }
        }),
        auth: user.isSignedIn
    });
    dispatch(
        checkoutReceiptActions.setOrderInformation({
        id: response,
        billing_address
    })
    );
    
    // Clear out everything we've saved about this cart from
    local storage.
    await clearBillingAddress();
    await clearCartId();
    await clearPaymentMethod();
    await clearShippingAddress();
    await clearShippingMethod();
    await clearComment();
    await clearApolloCache([
    '$ROOT_QUERY.customerOrders',
    'CustomerOrder'
    ]); // TODO add new order to cache instead of clearing
    it
        dispatch(actions.order.accept(response)); // Dispatches an response message that everything went fine.
    } catch (error) { // Handles the error if something went wrong.
        dispatch(actions.order.reject(error.baseMessage)); // Dispatches an error message.
    }
};